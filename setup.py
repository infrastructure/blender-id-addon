#!/usr/bin/env python3
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
import os.path
import shutil
import sys
import zipfile

from distutils import log
from distutils.core import Command, DistutilsOptionError
from distutils.command.bdist import bdist
from distutils.command.install import install, INSTALL_SCHEMES
from distutils.command.install_egg_info import install_egg_info
from setuptools import setup, find_packages

# Prevents __pycache__ dirs from being created & packaged.
sys.dont_write_bytecode = True


# noinspection PyAttributeOutsideInit
class BundleWithBlender(Command):
    """Bundles the addon with Blender."""

    description = "bundles the addon with Blender"
    user_options = [
        ('path=', None, "Blender's addon path"),
    ]

    def initialize_options(self):
        self.path = None  # path of blender/release/scripts/addons.

    def finalize_options(self):
        if not self.path:
            raise DistutilsOptionError("'--path /blender/release/scripts/addons' must be given")
        self.path = os.path.abspath(self.path)

    def run(self):
        if not self.distribution.dist_files:
            raise DistutilsOptionError('No dist file created in earlier command')

        if len(self.distribution.dist_files) > 1:
            raise DistutilsOptionError('Multiple dist files created in earlier command')

        # Remove any pre-existing packages from the Blender addons.
        for pkg in self.distribution.packages:
            pkg_path = os.path.join(self.path, pkg)
            if not os.path.exists(pkg_path):
                continue
            log.info('removing %s', pkg_path)
            shutil.rmtree(pkg_path)

        log.info('unpacking %s in %s', ', '.join(self.distribution.packages), self.path)

        _, _, filename = self.distribution.dist_files[0]
        self.unzip(filename)

    def unzip(self, zip_filename):
        with zipfile.ZipFile(zip_filename) as zf:
            for member in zf.infolist():
                log.info('  - extracting %r', member.filename)
                zf.extract(member, self.path)


class BlenderAddonBdist(bdist):
    """Ensures that 'python setup.py bdist' creates a zip file."""

    def initialize_options(self):
        INSTALL_SCHEMES['unix_local']['data'] = '$base'

        super().initialize_options()
        self.formats = ['zip']
        self.plat_name = 'addon'  # use this instead of 'linux-x86_64' or similar.


class BlenderAddonInstall(install):
    """Ensures the module is placed at the root of the zip file."""

    def initialize_options(self):
        super().initialize_options()
        self.prefix = ''
        self.install_lib = ''


class AvoidEggInfo(install_egg_info):
    """Makes sure the egg-info directory is NOT created.

    If we skip this, the user's addon directory will be polluted by egg-info
    directories, which Blender doesn't use anyway.
    """

    def run(self):
        pass


setup(
    cmdclass={'bdist': BlenderAddonBdist,
              'install': BlenderAddonInstall,
              'install_egg_info': AvoidEggInfo,
              'bundle': BundleWithBlender,
              },
    name='blender_id',
    description='The Blender ID addon allows authentication of users.',
    version='2.1.0',
    author='Francesco Siddi, Sybren A. Stüvel',
    author_email='francesco@blender.org',
    packages=find_packages('.'),
    data_files=[('blender_id', ['README.md', 'CHANGELOG.md'])],
    scripts=[],
    url='https://developer.blender.org/diffusion/BIA/',
    license='GNU General Public License v2 or later (GPLv2+)',
    platforms='',
    classifiers=[
        'Intended Audience :: End Users/Desktop',
        'Operating System :: OS Independent',
        'Environment :: Plugins',
        'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
    ],
    zip_safe=True,
    install_requires=['requests>=2.8.1'],
)
